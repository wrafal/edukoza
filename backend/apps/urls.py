from django.urls import include, path
from rest_framework import routers
from apps.accounts.views import UserViewSet, GroupViewSet, ProfileViewSet
from apps.course.views import CourseViewSet
from apps.category.views import CategoryViewSet
from apps.rating.views import RatingViewSet
from apps.image_files.views import ImageFileViewSet
from apps.certificate.views import CertificateViewSet
from apps.transaction.views import TransactionViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('profile/', ProfileViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('course/', CourseViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('category/', CategoryViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('rating/', RatingViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('images/', ImageFileViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('certificate/', CertificateViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('transaction/', TransactionViewSet.as_view({'get': 'list', 'post': 'create'})),
]