from django.db import models
from apps.base.models import BaseModel
from django.contrib.auth.models import User
from apps.course.models import Course


class Transaction(BaseModel):
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.buyer)
