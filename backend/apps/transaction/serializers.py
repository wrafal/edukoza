from rest_framework import serializers
from apps.transaction.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('buyer', 'course')
