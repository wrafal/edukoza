from datetime import datetime
import uuid

from django.db import models


class BaseModel(models.Model):
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    deactivated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        pass

    def deactivate_and_save(self, save=True):
        self.is_active = False
        self.deactivated_at = datetime.now()

        if save:
            self.save()
