from rest_framework import viewsets
from apps.certificate.models import Certificate
from apps.certificate.serializers import CertificateSerializer


class CertificateViewSet(viewsets.ModelViewSet):
    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer
