from django.contrib import admin
from apps.certificate.models import Certificate

admin.site.register(Certificate)
