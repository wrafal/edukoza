from django.db import models
from django.contrib.auth.models import User
from apps.base.models import BaseModel
from apps.image_files.models import ImageFile


class Certificate(BaseModel):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(max_length=1000)
    certificate_image = models.ForeignKey(ImageFile, on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.name)
