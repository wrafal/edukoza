from django.db import models
from apps.base.models import BaseModel
from django.contrib.auth.models import User
from apps.image_files.models import ImageFile


class Profile(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_image = models.ForeignKey(ImageFile, on_delete=models.CASCADE)
    want_to_teach = models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.user.username)
