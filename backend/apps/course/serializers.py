from rest_framework import serializers
from apps.course.models import Course


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = (
            'name',
            'author',
            'category',
            'course_rating',
            'description',
            'course_image',
            'date_from',
            'date_to',
            'price',
            'max_number_memberships',
            'difficulty_level',
            'camera',
            'microphone',
            'address'
        )
