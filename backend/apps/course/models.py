from django.db import models
from apps.base.models import BaseModel
from django.contrib.auth.models import User
from apps.category.models import Category
from apps.rating.models import Rating
from apps.image_files.models import ImageFile


class Course(BaseModel):
    name = models.CharField(max_length=200)
    author = models.OneToOneField(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    course_rating = models.ForeignKey(Rating, on_delete=models.CASCADE)
    description = models.TextField(max_length=1000)
    course_image = models.ForeignKey(ImageFile, on_delete=models.CASCADE)
    date_from = models.DateTimeField()
    date_to = models.DateTimeField()
    price = models.IntegerField()
    max_number_memberships = models.IntegerField()
    difficulty_level = models.IntegerField()
    camera = models.BooleanField(default=False)
    microphone = models.BooleanField(default=False)
    address = models.URLField()

    def __unicode__(self):
        return str(self.name)
