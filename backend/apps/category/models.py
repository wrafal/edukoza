from django.db import models
from apps.base.models import BaseModel


class Category(BaseModel):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return str(self.name)
