from rest_framework import viewsets
from apps.rating.models import Rating
from apps.rating.serializers import RatingSerializer


class RatingViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
