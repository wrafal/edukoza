from rest_framework import serializers
from apps.rating.models import Rating


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = ('information_transfer', 'competences', 'involve')
