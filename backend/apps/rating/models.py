from django.db import models
from apps.base.models import BaseModel


class Rating(BaseModel):
    information_transfer = models.FloatField(max_length=4)
    competences = models.FloatField(max_length=4)
    involve = models.FloatField(max_length=4)

    def __unicode__(self):
        return str(self.information_transfer)
