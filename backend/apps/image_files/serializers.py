from rest_framework import serializers
from apps.image_files.models import ImageFile


class ImageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageFile
        fields = ('file', 'name')
