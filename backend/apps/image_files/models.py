from django.db import models
from apps.base.models import BaseModel


class ImageFile(BaseModel):
    file = models.ImageField(blank=True, null=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return str(self.name)
