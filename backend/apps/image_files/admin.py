from django.contrib import admin
from apps.image_files.models import ImageFile

admin.site.register(ImageFile)
