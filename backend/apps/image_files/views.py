from rest_framework import viewsets
from apps.image_files.models import ImageFile
from apps.image_files.serializers import ImageFileSerializer


class ImageFileViewSet(viewsets.ModelViewSet):
    queryset = ImageFile.objects.all()
    serializer_class = ImageFileSerializer
