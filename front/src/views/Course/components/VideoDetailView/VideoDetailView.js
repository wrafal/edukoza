import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import ProgressComponent from '@material-ui/core/CircularProgress';
import {
  Card,
  CardContent,
  CardActions,
  Divider,
  Button,
  CardMedia
} from '@material-ui/core';
// import {JitsiMeetExternalAPI} from '../../../../client/external_api.js';

const useStyles = makeStyles(() => ({
  root: {}
}));

const VideoDetailView = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [scripReady, setScripReady] = useState(true);

  useEffect(() => {
    const script = document.createElement('script');
  
    script.src = "https://meet.jit.si/external_api.js";
    script.async = true;
  
    document.body.appendChild(script);
    setScripReady(true);
    return () => {
      document.body.removeChild(script);
    }

  }, []);

  const jitsiContainerStyle = {
    display: (loading ? 'none' : 'block'),
    width: '100%',
    height: '100%',
  }

  function startConference() {
    if(scripReady) {
      try {
        const domain = 'call.edukoza.pl';
        const options = {
          roomName: 'roomName',
          height: 700,
          parentNode: document.getElementById('jitsi-container'),
          interfaceConfigOverwrite: {
            filmStripOnly: false,
            SHOW_JITSI_WATERMARK: true,
          },
          configOverwrite: {
            disableSimulcast: false,
          },
        };
    
        const api = new window.JitsiMeetExternalAPI(domain, options);
        api.addEventListener('videoConferenceJoined', () => {
          console.log('Local User Joined');
          setLoading(false);
          api.executeCommand('displayName', 'TestNameToChange');
        });
      } catch (error) {
        console.error('Failed to load Jitsi API', error);
      }
    }
  }

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <CardMedia
          img={'/images/avatars/avatar_11.png'}
        />
        {loading && <ProgressComponent />}
        <div
          id="jitsi-container"
          style={jitsiContainerStyle}
        />
      </CardContent>
      <Divider />
      <CardActions style={{ width: '100%', textAlign: 'right' }}>
        <Button
          color="primary"
          variant="contained"
          onClick={() => { startConference(); }}
        >
          Dołacz
        </Button>
      </CardActions>
    </Card>
  );
};

VideoDetailView.propTypes = {
  className: PropTypes.string
};

export default VideoDetailView;
