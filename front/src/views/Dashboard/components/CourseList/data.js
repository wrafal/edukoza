import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    name: 'Web programming',
    date: '1/04/2020 12:30',
    category: 'Informatics',
    status: 'Planned',
    duration: '120 minutes'
  },
  {
    id: uuid(),
    name: 'SQL basics',
    date: '5/04/2020 12:30',
    category: 'Informatics',
    status: 'Planned',
    duration: '20 minutes'
  },
  {
    id: uuid(),
    name: 'UI/UX 101',
    date: '2/04/2020 12:30',
    category: 'Design',
    status: 'Ended',
    duration: '40 minutes'
  },
  {
    id: uuid(),
    name: 'Doradztwo zawodowe',
    date: '2/04/2020 12:30',
    category: 'Specialists',
    status: 'Ended',
    duration: '40 minutes'
  },
  {
    id: uuid(),
    name: 'Scratch - 101',
    date: '2/03/2020 12:30',
    category: 'For kids',
    status: 'Ended',
    duration: '40 minutes'
  },
  {
    id: uuid(),
    name: 'Git - version control 101',
    date: '3/04/2020 12:30',
    category: 'Informatics',
    status: 'Ended',
    duration: '40 minutes'
  }
];
