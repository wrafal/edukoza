import React from 'react';
import { Card, CardContent, CardMedia, Grid, Typography } from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import ExtensionIcon from '@material-ui/icons/Extension';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Rating from '@material-ui/lab/Rating';
import { withStyles } from '@material-ui/core/styles';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { makeStyles } from '@material-ui/core/styles';


const StyledRating = withStyles({
  iconFilled: {
    color: '#ff6d75',
  },
  iconHover: {
    color: '#ff3d47',
  },
})(Rating);

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  // avatar: {
  //   backgroundColor: red[500],
  // },
}));


const CourseTab = (props) => {
  const classes = useStyles();

  return (
    <Card>
    <CardMedia
        className={classes.media}
        style={{ opacity: '0.8'}}
        image={props.image} 
        title={props.title}
      />
      <CardContent>
        <Grid
          container
          // justify="space-between"
        >
          <Grid item xs={12}>
            <Typography variant="h3">{props.title}</Typography>
                <Typography
                color="textSecondary"
                gutterBottom
                variant="body2"
                >
                {props.description}
                </Typography>
            </Grid>
            <Grid item xs={3}>
                <ExtensionIcon/>
                <Typography variant="h5">{props.dificulty}</Typography>
            </Grid>
            <Grid item xs={3}>
                <AccessTimeIcon/>
                <Typography variant="h5">{props.time}h</Typography>
            </Grid>
            <Grid item xs={3}>
                <MonetizationOnIcon/>
                <Typography variant="h5">{props.price} zł</Typography>
            </Grid>
            <Grid item xs={3}>
                <StyledRating
                  name="customized-color"
                  defaultValue={props.opinion}
                  getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
                  precision={0.5}
                  readOnly={true}
                  icon={<FavoriteIcon />}
                />
            </Grid>
          
          
          
        </Grid>
        
      </CardContent>
    </Card>
  );
};

export default CourseTab;
