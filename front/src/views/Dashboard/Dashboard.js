import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Row } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CourseTab from './components/CourseTab';
import CourseData from './components/CourseData';
import SearchIcon from '@material-ui/icons/Search';
import Grow from '@material-ui/core/Grow';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  inputRoot: {
    background: "black"
  },
  input: {
    color: "white",
  },
  notchedOutline: {
    borderWidth: "1px",
    borderColor: "#00e59e !important"
  },
  floatingLabelFocusStyle: {
    color: "#00e59e"
  }
}));


const CourseTile = CourseData.map(Course => 
  <Grow in={true} key={Course.id} component={Link} to="/course">
    <Grid
      item
      lg={3}
      sm={6}
      xl={3}
      xs={12}
    >
      <CourseTab 
        title = {Course.title}
        author = {Course.author}
        description = {Course.description}
        dificulty = {Course.dificulty}
        date = {Course.date}
        price = {Course.price}
        time = {Course.time}
        opinion = {Course.opinion}
        image = {Course.image}
        
      />
    </Grid>
  </Grow>
);

const SearchTextField = withStyles({
  root: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: `10px 10px  10px  10px`,
      },
      background: "black",
      borderColor: "yellow !important"
    },
  },
  floatingLabelFocusStyle: {
    color: "#00e59e"
  }
})(TextField);

const Dashboard = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={3}
        lg={12}
          md={12}
          xl={12}
          xs={12}
      >
      <Grow in={true}>
        <Grid
          container
          spacing={3}
        >
          <Grid item 
            lg={9}
            md={9}
            xl={9}
            xs={9}>
            <SearchTextField 
              label="Search course"
              variant="outlined"
              floatingLabelFocusStyle={classes.floatingLabelFocusStyle}
              InputProps={{
                className: classes.input,
                notchedOutline: classes.notchedOutline
              }}
            />
          </Grid>
          <Grid item 
            lg={3}
            md={3}
            xl={3}
            xs={3}>
            <Button variant="contained"><SearchIcon/>Search</Button>
          </Grid>
          </Grid>
      </Grow>
        {CourseTile}
      </Grid>
    </div>
  );
};

export default Dashboard;