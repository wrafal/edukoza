import React from 'react';
import { Card, CardContent, Grid, Typography } from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import ExtensionIcon from '@material-ui/icons/Extension';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';


const CourseTab = (props) => {
  
  return (
    <Card>
      <CardContent>
        <Grid
          container
          justify="space-between"
        >
          <Grid item xs={12}>
              <img src = {props.image} alt= "Course poster"/>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h1">{props.title}</Typography>
                <Typography
                color="textSecondary"
                gutterBottom
                variant="body2"
                >
                {props.description}
                </Typography>
            </Grid>
            <Grid item xs={3}>
                <ExtensionIcon/>
                <Typography variant="h5">{props.dificulty}</Typography>
            </Grid>
            <Grid item xs={3}>
                <AccessTimeIcon/>
                <Typography variant="h5">{props.time}h</Typography>
            </Grid>
            <Grid item xs={3}>
                <MonetizationOnIcon/>
                <Typography variant="h5">{props.price} zł</Typography>
            </Grid>
            <Grid item xs={3}>
                <Typography variant="h5">Ranked: <br/> {props.opinion}/5</Typography>
            </Grid>
          
          
          
        </Grid>
        
      </CardContent>
    </Card>
  );
};

export default CourseTab;
