import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { 
  Main as MainLayout,
  Minimal as MinimalLayout,
  SearchMain as SearchMainLayout 
} from './layouts';

import {
  Dashboard as DashboardView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  Course as CourseView,
  TrainerDashboard as TrainerDashboardView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout
        component={CourseView}
        exact
        layout={MainLayout}
        path="/course"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={SearchMainLayout}
        path="/"
      />
      <RouteWithLayout
        component={TrainerDashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={AccountView}
        exact
        layout={MainLayout}
        path="/account"
      />
      <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path="/settings"
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />

      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
