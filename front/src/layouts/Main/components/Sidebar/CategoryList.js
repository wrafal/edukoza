import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import TranslateIcon from '@material-ui/icons/Translate';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import PanToolIcon from '@material-ui/icons/PanTool';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import SportsTennisIcon from '@material-ui/icons/SportsTennis';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import ComputerIcon from '@material-ui/icons/Computer';
import PaletteIcon from '@material-ui/icons/Palette';
import GavelIcon from '@material-ui/icons/Gavel';
import FaceIcon from '@material-ui/icons/Face';
import BrushIcon from '@material-ui/icons/Brush';
import ChildCareIcon from '@material-ui/icons/ChildCare';
import WorkIcon from '@material-ui/icons/Work';
import ClearAllIcon from '@material-ui/icons/ClearAll';



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  icon:{
    marginRight: 6,
  },
}));

function CategoryList() {
  const classes = useStyles();
  
  const [langList, setLangList] = React.useState(false);
  const selectLanguage = () => {
    setLangList(!langList);
  };

  const [subList, setSubList] = React.useState(false);
  const selectSubject = () => {
    setSubList(!subList);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Courses category:
        </ListSubheader>
      }
      className={classes.root}
    >
       <ListItem button onClick={selectLanguage}>
        <TranslateIcon className={classes.icon}/>
        <ListItemText primary="Languages" />
        {langList ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={langList} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemText primary="Polish" />
          </ListItem>
          
          <ListItem button className={classes.nested}>
            <ListItemText primary="English" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="German" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="French" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="Spanish" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="Others" />
          </ListItem>
        </List>
      </Collapse>

      <ListItem button onClick = {selectSubject}>
        <ImportContactsIcon className={classes.icon}/>
        <ListItemText primary="School" />
        {subList ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={subList} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemText primary="Math" />
          </ListItem>
          
          <ListItem button className={classes.nested}>
            <ListItemText primary="Phisics" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="Biology" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="Chemistry" />
          </ListItem>

          <ListItem button className={classes.nested}>
            <ListItemText primary="Geography" />
          </ListItem>
        </List>
      </Collapse>

      <ListItem button>
        <PanToolIcon className={classes.icon}/>
        <ListItemText primary="DIY"/>
      </ListItem>

      <ListItem button>
        <MusicNoteIcon className={classes.icon}/>
        <ListItemText primary="Music"/>
      </ListItem>

      <ListItem button>
        <SportsTennisIcon className={classes.icon}/>
        <ListItemText primary="Sports"/>
      </ListItem>

      <ListItem button>
        <RecordVoiceOverIcon className={classes.icon}/>
        <ListItemText primary="Standups"/>
      </ListItem>

      <ListItem button>
        <ComputerIcon className={classes.icon}/>
        <ListItemText primary="IT"/>
      </ListItem>

      <ListItem button>
        <PaletteIcon className={classes.icon}/>
        <ListItemText primary="Design"/>
      </ListItem>

      <ListItem button>
        <GavelIcon className={classes.icon}/>
        <ListItemText primary="Law"/>
      </ListItem>

      <ListItem button>
        <FaceIcon className={classes.icon}/>
        <ListItemText primary="Beauty"/>
      </ListItem>

      <ListItem button>
        <BrushIcon className={classes.icon}/>
        <ListItemText primary="Art"/>
      </ListItem>

      <ListItem button>
        <ChildCareIcon className={classes.icon}/>
        <ListItemText primary="Kids"/>
      </ListItem>

      <ListItem button>
        <WorkIcon className={classes.icon}/>
        <ListItemText primary="Specjalists"/>
      </ListItem>

      <ListItem button>
        <ClearAllIcon className={classes.icon}/>
        <ListItemText primary="Others"/>
      </ListItem>
    </List>
  );
}
export default CategoryList;