Repository for 2o20 hackaton HackYeah !
Repository contains

front - frontend React JS application with Material-UI framework
  packed as docker image and deployet to rost IO by CI/CD from gitlab.
  Frontend is also integrated with video/audio streaming platform
  (see video-streaming)
  Deployed version is available at: https://edukoza.pl
  Landin page is available: https://edukoza.com or(https://www.landpage.co/d076e376-7683-11ea-803c-0ad51e94bc5a)
  
backend - python/Django application containing all needed models. Endpoints
  via REST approach. Packed with Docker and deployed by Rancher via Gitlab CI/CD
  We use PostrgressDB as main db
 
video-streaming - we packed and reconfigured Jitsee Meet free opensource 
  video/audio streaming and chat application for our purpose.
  Applicaiton is availabe in https://call.edukoza.pl
 
